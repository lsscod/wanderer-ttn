-- DATABASE cbldb
-- execute as: psql -U pioann cbldb -a -f SQLIoT.sql

DROP TABLE iotdata;
DROP TABLE bikes;
DROP TABLE customers;


CREATE TABLE iotdata(
rec_id SERIAL PRIMARY KEY,
bike_id INT NOT NULL,
gps_x FLOAT (6) NOT NULL,
gps_y FLOAT (6) NOT NULL,
status SMALLINT,
ts INT,
info VARCHAR(256), 
created_at TIMESTAMP);

CREATE TABLE bikes(
bike_id SERIAL PRIMARY KEY,
cust_id INT,
deviceid VARCHAR(128) UNIQUE,
name VARCHAR(128),
info VARCHAR(256),
status SMALLINT NOT NULL,
updated_at TIMESTAMP NOT NULL);

CREATE TABLE customers(
cust_id SERIAL PRIMARY KEY,
name VARCHAR(128),
info VARCHAR(256),
updated_at TIMESTAMP NOT NULL);


-- Insert data into the tables
-------------------------------------------------------------------

-- create bikes
INSERT INTO bikes (cust_id, deviceid, name, info, status, updated_at) VALUES (1, 'bike_batman_01', 'batman bike', '{}', 1, '2018-12-08 12:02:14');
INSERT INTO bikes (cust_id, deviceid, name, info, status, updated_at) VALUES (2, '#f4897k', 'robin bike', '{}', 0, '2018-12-02 06:13:01');
INSERT INTO bikes (cust_id, deviceid, name, info, status, updated_at) VALUES (4, '#t2492i', 'catwoman bike', '{}', 2, '2018-11-19 22:19:14');
INSERT INTO bikes (cust_id, deviceid, name, info, status, updated_at) VALUES (3, '#p8273o', 'kiwi bike', '{}', 3, '2018-09-27 14:21:00');
INSERT INTO bikes (cust_id, deviceid, name, info, status, updated_at) VALUES (3, '#a0000a', 'noname', '{}', 0, '2018-09-27 10:18:00');
INSERT INTO bikes (cust_id, deviceid, name, info, status, updated_at) VALUES (5, '#supaman3', 'superman', '{}', 0, '2018-03-27 14:48:00');
INSERT INTO bikes (cust_id, deviceid, name, info, status, updated_at) VALUES (3, '#hitch9', 'alfred', '{}', 0, '2018-08-17 15:28:00');

-- create customers
INSERT INTO customers (name, info, updated_at) VALUES ('John Doe', '{}', '2018-12-08 16:04:00');
INSERT INTO customers (name, info, updated_at) VALUES ('Alice Cooper', '{}', '2018-12-08 08:00:00');
INSERT INTO customers (name, info, updated_at) VALUES ('CBL', '{}', '2018-12-02 07:00:00');
INSERT INTO customers (name, info, updated_at) VALUES ('Cool Pinapple', '{}', '2018-12-02 17:20:20');
INSERT INTO customers (name, info, updated_at) VALUES ('Bruce Dickinson', '{}', '2018-12-14 13:47:56');

-- create iotdata
INSERT INTO iotdata (bike_id, gps_x, gps_y, status, ts, info, created_at) VALUES (1, 56.1314, 10.5431, 0, 1544092949, '{}', '2018-12-02 17:20:20');
INSERT INTO iotdata (bike_id, gps_x, gps_y, status, ts, info, created_at) VALUES (1, 56.4234, 10.4632, 0, 1544112948, '{}', '2018-12-04 06:48:21');
INSERT INTO iotdata (bike_id, gps_x, gps_y, status, ts, info, created_at) VALUES (3, 56.1772, 10.6123, 1, 1544142233, '{}', '2018-12-05 07:56:40');
INSERT INTO iotdata (bike_id, gps_x, gps_y, status, ts, info, created_at) VALUES (2, 56.2001, 10.4201, 0, 1544192461, '{}', '2018-12-06 10:01:13');
INSERT INTO iotdata (bike_id, gps_x, gps_y, status, ts, info, created_at) VALUES (4, 56.2191, 10.5230, 0, 1544252812, '{}', '2018-12-07 12:21:30');
INSERT INTO iotdata (bike_id, gps_x, gps_y, status, ts, info, created_at) VALUES (2, 56.1103, 10.6012, 1, 1544262975, '{}', '2018-12-07 12:50:21');
INSERT INTO iotdata (bike_id, gps_x, gps_y, status, ts, info, created_at) VALUES (1, 56.1283, 10.5219, 0, 1544283417, '{}', '2018-12-08 16:37:20');

