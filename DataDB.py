import psycopg2


class DataDB:
    def __init__(self):
        print("Connecting to database...")
        self.conn = psycopg2.connect(database="cbldb", user="pioann", password="pioann",
                                     host="127.0.0.1", port="5432")
        self.cur = self.conn.cursor()

    def insert_data(self, dev_id, data):
        print("Inserting data:", dev_id, data)
        query = "SELECT bike_id FROM bikes WHERE deviceid = %s"
        self.cur.execute(query, (dev_id,))
        bike_id = self.cur.fetchone()[0]

        self.cur.execute(
            "INSERT INTO iotdata (bike_id, gps_x, gps_y, status, ts, info, created_at) "
            "VALUES (%s, %s, %s, %s, %s, %s, CURRENT_TIMESTAMP) RETURNING *",
            (bike_id, data[0], data[1], data[3], data[2], '{}')
        )
        res = self.cur.fetchone()
        self.conn.commit()
        print("Inserted in DB: ", res)

    def close(self):
        self.cur.close()
        self.conn.close()
