import paho.mqtt.client as mqtt
import json
import base64
from Config import Config
from DataDB import DataDB


class TTN:
    def __init__(self):
        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.username_pw_set(Config.APP_ID, password=Config.ACCESS_KEY)
        self.device_id = None
        self.message = []
        self.db = DataDB()

    def connect(self):
        self.client.connect(Config.HOST, 1883, 60)

    def run(self):
        self.client.loop_forever()

    @staticmethod
    def on_connect(ttn_client, userdata, flags, rc):
        if rc == 0:
            print("Connected to TTN, with status code: " + str(rc) + "\n")
            ttn_client.subscribe(Config.TOPIC)
        else:
            print("Trying to connect... (status code: {})".format(str(rc)))

    def on_message(self, ttn_client, userdata, msg):
        m = json.loads(msg.payload.decode("utf-8"))
        payload_raw = m["payload_raw"]
        device_id = m["dev_id"]
        res = base64.b64decode(payload_raw).decode('ascii')
        f = res.split(' ')
        self.device_id = device_id
        self.message = f
        self.print()
        self.db.insert_data(device_id, f)

    def get_device_id(self):
        return self.device_id

    def get_message(self):
        return self.message

    def print(self):
        print("Dev_id:", self.get_device_id())
        f = self.get_message()
        print("Array: {} - {} - {} - {}".format(f[0], f[1], f[2], f[3]))

    def close(self):
        self.db.close()
